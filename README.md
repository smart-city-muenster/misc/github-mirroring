# Repository Mirroring CI/CD Example

## Zielsetzung
Wird ein Projekt auf einer anderen git-basierten Plattform weiterentwickelt und soll nach Open CoDE gespiegelt werden, stellt das so genannte Pull-Mirroring bei vielen Nutzenden eine Herausforderung dar.
Dieses Repository zeigt als Proof-of-Concept eine Möglichkeit zur Umsetzung des Pull-Mirrorings via einer CI/CD Pipeline, welche automatisiert das Quell-Repository kopiert und in ein Zielrepository auf Open CoDE committet.

## Konfiguration
In der CI/CD Pipeline werden einige Variablen benötigt, welche die Nutzenden vor dem Einsatz der Pipeline definieren müssen. Weiterhin ist ein Helfer-Repository und ein Zielrepository notwendig.

- Anlegen eines Ziel-Repository auf Open CoDE.
- Anlegen eines Helfer-Repository in der die CI/CD Pipeline für ein oder mehr Projekte zur Spiegelung läuft.

### Aktionen im Ziel-Repository

1. Erstellen Sie das Ziel-Projekt auf Open CoDE (komplett leer, also auch ohne README, entsrpechenden Haken bei der Projekterstellung entfernen!)
2. Erstellen eines Project Access Tokens mit mindestens der Rolle "Maintainer" und den Berechtigungen `read_repository` und `write_repository` Berechtigungen entsprechend der [Dokumentation](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token) von GitLab (Settings &rarr; Access Tokens).
3. Die Access Tokens sind maximal 365 Tage gültig und müssen danach erneuert werden.

Möchten Sie nicht nur ein Reporitory, sondern zwei oder mehr nach Open CoDE spiegeln, so erstellen Sie mehrere Ziel-Repositories sowie Project Access Tokens und modifizieren die `gitlab-ci.yml` entsprechend mit zusätzlichen Variablen.

### Aktionen im Helfer-Repository mit CI/CD Pipeline

3. Anlegen der [.gitlab-ci.yml](.gitlab-ci.yml) entsprechend der Vorlage in diesem Repository oder mit ihren eigenen Anpassungen
4. Hinzufügen der CI/CD Variablen unter: 
    - Settings &rarr; CI/CD &rarr; Variables (expand) &rarr; Add variable
5. Erstellen und Befüllen der CI/CD Variablen nach folgendem Muster:

| Variablenname | Variable | Hinweis |
|---|---|---|
| GIT_USER_NAME | Ihr Benutzername ist ersichtlich bei Klick auf Ihr Profilbild, Eingabe ohne das vorangehende @ | |
| GIT_USER_EMAIL | Ihre zugehörige Mailadresse ist ersichtlich bei Klick auf Ihr Profilbild &rarr; Edit Profile &rarr; Profile &rarr; Main settings &rarr; Email | |
| SOURCE_REPOSITORY | Der Link zu dem zu spiegelnden Repository in einem anderen git-basierte System z.B GitHub https://github.com/[...]| Der tatsächliche Web-Link, kein .git Pfad |
| DESTINATION_REPOSITORY | Der Link zu Ihrem Repository im Open CoDE GitLab gitlab.opencode.de/[...]/[...] | Kein führendes `https://` eingeben, direkt mit `gitlab` starten. Und: kein .git Pfad |
| TARGET_ACCESS_TOKEN | Project Access Tokens aus Schritt 2 | Token muss Berechtigungen `read_repository` und `write_repository` sowie mindestens die Rolle "Maintainer" haben |

## Einsatz der Pipeline
### Einmaliger Einsatz
Einmaliges Starten der Pipeline geschieht initial bereits automatisch bei der Erstellung der gitlab-ci.yml.

Einmaliges Starten:
- Auf der Startseite des Projekts A &rarr; CI/CD &rarr; Pipelines &rarr; Run pipeline &rarr; Run pipeline

### Regelmäßiger Einsatz
Für ein automatisches regelmäßiges Spiegeln können Sie einen Schedule für Ihre Pipeline erstellen.
- Auf der Startseite des Projekts A &rarr; CI/CD &rarr; Schedule &rarr; New Schedule &rarr; Beschreibung, Intervall, ... ausfüllen &rarr; Save pipeline schedule
